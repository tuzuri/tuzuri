'use strict';

export const index = () => {
  const quickScanBtn = <HTMLImageElement>document.getElementById('quick-scan');
  const fullScanBtn = <HTMLImageElement>document.getElementById('full-scan');

  const quickScan = () => {
    let icon = <HTMLImageElement>quickScanBtn.firstChild;
    icon.classList.add('rotate');
    fetch('api/v1/quick-scan')
      .then((response) => {
        console.log('quick scan sucsess');
        location.reload();
      })
      .catch((error) => {
        console.log(error);
        icon.classList.remove('rotate');
      });
  }

  const fullScan = () => {
    let icon = <HTMLImageElement>fullScanBtn.firstChild;
    icon.classList.add('rotate');
    fetch('api/v1/full-scan')
      .then((response) => {
        console.log('full scan sucsess');
        location.reload();
      })
      .catch((error) => {
        console.log(error);
        icon.classList.remove('rotate');
      });
  }

  quickScanBtn.addEventListener('click', quickScan);
  fullScanBtn.addEventListener('click', fullScan);
}
