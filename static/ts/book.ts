'use strict';

import * as metadata  from "./metadata";
import * as pagedata  from "./pagedata";
import * as thumbnail from "./thumbnail";

export const book = async () => {
  const toIndex = (i: number) => i - 1;
  const toPageNum = (i: number) => i + 1;
  const nextPage = async () => await changePage(presentPageNum + 1);
  const prevPage = async () => await changePage(presentPageNum - 1);

  const loopOverflowNum = (target: number, min: number, max: number): number => {
    target =  (min > target) ? max : target;
    target =  (max < target) ? min : target;
    return target;
  }

  const changePage = async (target: number) => {
    target = loopOverflowNum(target, 1, maxPageNum);
    presentImage.src = imageThumbs[toIndex(target)].src;

    imageThumbs[toIndex(presentPageNum)].classList.remove('present');
    imageThumbs[toIndex(target)].classList.add('present');

    presentPageNum = target;
    pageId = imageThumbs[toIndex(presentPageNum)].dataset.braidid as string;

    readingProgress(presentPageNum, maxPageNum);
    thumbnail.handleHidden(presentPageNum);
    metadata.showMetadata(presentPageNum);
    pageData = await pagedata.fetchPageData(pageId);
    await pagedata.showPageData(pageData);
    await fetchLove();
  }

  const readingProgress = (presentPageNum: number, maxPageNum: number) => {
    const percent = Math.floor((presentPageNum / maxPageNum) * 100);
    progress.textContent = ` / ${maxPageNum} (${percent}%)`;
    inputPageNum.value = presentPageNum.toString();
    inputPageNum.max = maxPageNum.toString();
  }

  const toggleLove = () => {
    love.classList.toggle('love-active');
    updateLove(love.classList.contains('love-active'));
  }

  const updateLove = (love: boolean) => {
    // TODO
  }

  const isLove = async () => {
    let tag_ids = await pageData.user_tag_ids;
    if (tag_ids == null) { return false }
    return tag_ids.includes(1)
  }

  const fetchLove = async () => {
    if (await isLove()) {
      love.classList.add('love-active');
    } else {
      love.classList.remove('love-active');
    }
  }

  const handleKeyDown = () => {
    document.addEventListener('keydown', async (event) => {
      switch (event.key) {
        case 'j':
        case 'ArrowRight':
          nextPage();
          break;
        case 'k':
        case 'ArrowLeft':
          prevPage();
          break;
        case 'b':
          document.location.href = '/';
          break;
        case 'l':
          toggleLove();
          break;
        case 'n':
          focusInputPageNumber();
          break;
        case 'v':
          presentImage.scrollIntoView({ behavior: "smooth", block: "end" });
          break;
        case 't':
          scrollTo(bar);
          break;
        case 'i':
          scrollTo(imageMeta);
          break;
        case 'p':
          scrollTo(pageMeta);
          break;
      }
    })
  }

  const focusInputPageNumber = () => {
    // sleep to prevent entered shortcut keys from overwriting the page number
    setTimeout(() => {
      inputPageNum.focus({ preventScroll: true });
      scrollTo(status);
    }, 100);
  }

  const scrollTo = (element: HTMLElement) => {
    if (isHiddenAbove(element)) {
      element.scrollIntoView({ behavior: "smooth", block: "start" });
    }
    if (isHiddenBelow(element)) {
      element.scrollIntoView({ behavior: "smooth", block: "end" });
    }
  }

  const isHiddenAbove = (element: HTMLElement) => {
    let rect = element.getBoundingClientRect();
    return (rect.top < 0);
  }

  const isHiddenBelow = (element: HTMLElement) => {
    let rect = element.getBoundingClientRect();
    return (rect.bottom > window.innerHeight);
  }

  const imageThumbs = document.querySelectorAll<HTMLImageElement>('.thumbnail');
  const presentImage = <HTMLImageElement>document.getElementById('present-image');

  const prev = <HTMLElement>document.getElementById('prev');
  const next = <HTMLElement>document.getElementById('next');
  const prevArea = <HTMLElement>document.getElementById('prev-area');
  const nextArea = <HTMLElement>document.getElementById('next-area');

  const progress = <HTMLElement>document.getElementById('progress');
  const inputPageNum = <HTMLInputElement>document.getElementById('input-page-num');
  const pageForm = <HTMLInputElement>document.getElementById('page-form');
  const status = <HTMLInputElement>document.getElementById('status');
  const bar = <HTMLInputElement>document.getElementById('bar');
  const imageMeta = <HTMLInputElement>document.getElementById('image-metadata');
  const pageMeta = <HTMLInputElement>document.getElementById('page-metadata');
  const love = document.querySelectorAll<HTMLElement>('.love')[0];

  const initialPageNum = 1;
  const maxPageNum = imageThumbs.length;
  let presentPageNum: number = initialPageNum;
  let pageId = imageThumbs[toIndex(presentPageNum)].dataset.braidid as string;
  let pageData: any = await pagedata.fetchPageData(pageId);

  readingProgress(presentPageNum, maxPageNum);
  handleKeyDown();
  thumbnail.handleHidden(presentPageNum);
  metadata.showMetadata(presentPageNum);
  await pagedata.showPageData(await pageData);
  await fetchLove();

  prevArea.addEventListener('click', prevPage);
  nextArea.addEventListener('click', nextPage);
  prev.addEventListener('click', prevPage);
  next.addEventListener('click', nextPage);
  love.addEventListener('click', toggleLove);

  pageForm.addEventListener("submit", async (event) => {
    event.preventDefault();
    await changePage(parseInt(inputPageNum.value));
    inputPageNum.blur();
    presentImage.scrollIntoView({ behavior: "smooth", block: "end" });
  });

  inputPageNum.addEventListener("focus", (event) => {
    (event.target as HTMLInputElement).select();
  });

  window.addEventListener('resize', () => {
    metadata.showMetadataFromImage();
    thumbnail.refresh();
    thumbnail.handleHidden(presentPageNum);
  });

  imageThumbs.forEach((thumb, i) => {
    thumb.addEventListener('click', async () => await changePage(toPageNum(i)))
  });
}
