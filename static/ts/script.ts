'use strict';

import * as Book from "./book";
import * as Index from "./index";

switch (window.location.pathname.split('/').slice(-1)[0]) {
    case "":
    case "test_index.html":
        Index.index();
        break;
    case "test_images.html":
    default:
        Book.book();
}

