'use strict';

const imageThumbs = document.querySelectorAll<HTMLImageElement>('.thumbnail');
const barItems = document.querySelectorAll<HTMLElement>('.thumbnail-li');
const maxPageNum = imageThumbs.length;

const isOdd = (n: number) => n % 2 == 1;
const toIndex = (i: number) => i - 1;

const setShowThumbsNum = () => {
  let windowWidth = document.documentElement.clientWidth;
  let thumbsWidth = 60;
  let thumbsGap = 5;
  let buttonWidth = 50;
  let n = Math.trunc((windowWidth - (buttonWidth * 2)) / (thumbsWidth + (thumbsGap * 2)));
  return isOdd(n)? n : n - 1;
}

const stopOverflowNum = (target: number, min: number, max: number): number => {
  return Math.max(Math.min(target, max), min);
}

export const handleHidden = (present: number) => {
  let start = stopOverflowNum((present - prevBuffer), 1, maxPageNum);
  let end = stopOverflowNum((present + nextBuffer), 1, maxPageNum);

  // if the number of thumbnails is less than `showThumbsNum`, add the missing amount
  start = ((maxPageNum - start) < showThumbsNum) ? (maxPageNum - showThumbsNum + 1) : start;
  end = (end < showThumbsNum) ? showThumbsNum : end;

  // update hidden attribute
  for(let i = 0; i < maxPageNum; i++) {
    barItems[i].hidden = (i < toIndex(start) || i > toIndex(end));
  }
}

export const refresh = () => {
  showThumbsNum = setShowThumbsNum();
  nextBuffer = (showThumbsNum - 1) / 2;
  prevBuffer = (showThumbsNum - 1) / 2;
}

let showThumbsNum = setShowThumbsNum();
let nextBuffer = (showThumbsNum - 1) / 2;
let prevBuffer = (showThumbsNum - 1) / 2;
