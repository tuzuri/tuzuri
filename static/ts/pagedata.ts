'use strict';

const pageIdElem = <HTMLImageElement>document.getElementById('page-id');
const authors = <HTMLImageElement>document.getElementById('authors');
const organization = <HTMLImageElement>document.getElementById('organization');
const userTags = <HTMLImageElement>document.getElementById('user-tags');
const userComments = <HTMLImageElement>document.getElementById('user-comments');

export const showPageData = (pageData: any) => {
    pageIdElem.textContent = pageData.id;
    authors.textContent = pageData.author_ids;
    organization.textContent = pageData.organization_ids;
    userTags.textContent = pageData.user_tag_ids;
    userComments.textContent = pageData.user_comment;
}

export const fetchPageData = async (pageId: string) => {
    return await fetchJSON(`api/v1/page-data/${pageId}`);
}

const fetchJSON = async (url: string) => {
    const data = await fetch(url)
        .then((response) => response.json())
        .then((data) => data);
    return data;
}
