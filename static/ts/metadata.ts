'use strict';

const imageThumbs = document.querySelectorAll<HTMLImageElement>('.thumbnail');
const presentImage = <HTMLImageElement>document.getElementById('present-image');
const displayDimensions = <HTMLInputElement>document.getElementById('display-dimensions');
const dimensions = <HTMLInputElement>document.getElementById('dimensions');
const hash = <HTMLInputElement>document.getElementById('hash');
const quality = <HTMLInputElement>document.getElementById('quality');
const mime = <HTMLInputElement>document.getElementById('mime');
const capacity = <HTMLInputElement>document.getElementById('capacity');

const toIndex = (i: number) => i - 1;

export const showMetadata = (presentPageNum: number) => {
  showMIMEType(presentPageNum);
  showCapacity(presentPageNum);
  showHash(presentPageNum);
  showMetadataFromImage();
}

export const showMetadataFromImage = () => {
  let intervalId = setInterval( () => {
    if (presentImage.complete) {
      let displayDimensions= getDisplayDimensions();

      showDimensions();
      showDisplayDimensions(displayDimensions);
      showQuality(displayDimensions);
      clearInterval(intervalId);
    }
  })
}

const showHash = (presentPageNum: number) => {
  hash.textContent = imageThumbs[toIndex(presentPageNum)].id.split('-')[1];
}

const showDimensions = () => {
  dimensions.textContent = `${presentImage.naturalWidth}x${presentImage.naturalHeight}`;
}

const getDisplayDimensions = () => {
  let scale = window.devicePixelRatio;
  let w = Math.trunc(window.innerWidth * scale);
  let h = Math.trunc(window.innerHeight * scale);
  return new Map<string, number>([["width", w], ["height", h]]);
}

const showDisplayDimensions = (dd: Map<string, number>) => {
  displayDimensions.textContent = `${dd.get("width")}x${dd.get("height")}`;
}

const showQuality = (dd: Map<string, number>) => {
  let w = presentImage.naturalWidth - (dd.get("width") as number);
  let h = presentImage.naturalHeight - (dd.get("height") as number);
  let msg;

  if ((w + h) === 0) {
    msg = "dot by dot";
  } else if ((w > 0) && (h > 0)) {
    msg = "sufficient";
  } else {
    msg = "insufficient";
  }

  quality.textContent = msg;
}

const showMIMEType = (presentPageNum: number) => {
  mime.textContent = `${imageThumbs[toIndex(presentPageNum)].dataset.mime}`;
}

const showCapacity = (presentPageNum: number) => {
  let bytes = Number(imageThumbs[toIndex(presentPageNum)].dataset.capacity);
  capacity.textContent = conversionByteUnit(bytes);
}

const conversionByteUnit = (bytes: number) => {
  if (bytes >= 1073741824) {
    let gibibytes = bytes/1073741824;
    return `${truncateTwoDec(gibibytes)} GiB`;
  } else if (bytes >= 1048576) {
    let mebibytes = bytes/1048576;
    return `${truncateTwoDec(mebibytes)} MiB`;
  } else if (bytes >= 1024) {
    let kibibytes = bytes/1024;
    return `${truncateTwoDec(kibibytes)} KiB`;
  } else {
    return `${bytes} B`;
  }
}

const truncateTwoDec = (num: number) => { return Math.floor(num*100)/100; }

