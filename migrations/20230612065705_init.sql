CREATE TABLE users
(
    id                  SERIAL          PRIMARY KEY,
    user_name           TEXT            NOT NULL,
    passward            BYTEA           NOT NULL
    -- created_at          TIMESTAMP       NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE files
(
    sha3_512            BYTEA           PRIMARY KEY,
    file_path           TEXT            NOT NULL,
    mime_type           VARCHAR(30)     NOT NULL,
    mime_subtype        VARCHAR(30)     NOT NULL,
    capacity_byte       INTEGER         NOT NULL,
    -- added_at            TIMESTAMP       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    source              TEXT
);

CREATE TABLE images
(
    sha3_512            BYTEA           PRIMARY KEY,
    dimension_height    INTEGER         NOT NULL,
    dimension_width     INTEGER         NOT NULL
);

CREATE TABLE braids
(
    id                  SERIAL          PRIMARY KEY,
    kind                VARCHAR(50)     NOT NULL,
    sha3_512_hashs      BYTEA[],
    braid_ids           INTEGER[],
    author_ids          INTEGER[],
    organization_ids    INTEGER[],
    user_tag_ids        INTEGER[],
    book_title          TEXT,
    user_comment        TEXT
);

CREATE TABLE user_tags
(
    id                  SERIAL          PRIMARY KEY,
    tag_name            VARCHAR(100)     NOT NULL UNIQUE
);

CREATE TABLE authors
(
    id                  SERIAL          PRIMARY KEY,
    author_name         VARCHAR(100)    NOT NULL UNIQUE
);

CREATE TABLE organizations
(
    id                  SERIAL          PRIMARY KEY,
    organization_name   VARCHAR(100)    NOT NULL UNIQUE
);
