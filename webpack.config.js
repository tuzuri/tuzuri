module.exports = {
  mode: 'development', // development or production
  entry: './static/ts/script.ts',
  output: {
    path: `${__dirname}/static/js`,
    filename: "script.js"
  },
  devtool: 'eval-source-map',
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
      },
    ],
  },
  resolve: {
    extensions: [
      '.ts', '.js',
    ],
  },
};