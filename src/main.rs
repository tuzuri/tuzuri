use actix_files;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use dotenv::dotenv;
use scanner::{full_scan, quick_scan, FileInfo};
use sqlx::{postgres::PgPoolOptions, Pool, Postgres};
use std::collections::HashMap;
use tera::{Context, Tera};

use crate::db::*;
use crate::footer::*;

mod db;
mod footer;
mod scanner;

pub struct AppState {
    db: Pool<Postgres>,
}

async fn index(tera: web::Data<Tera>, data: web::Data<AppState>) -> impl Responder {
    let state: Pool<Postgres> = data.db.clone();
    let works: Vec<BraidWork> = fetch_works(&state).await;

    let mut works_map = Vec::new();
    for work in &works {
        let mut work_map = HashMap::new();
        let cover_braid_id: i32 = *work.braid_ids.first().unwrap();
        let cover_page: BraidPage = fetch_page_braid(&state, &cover_braid_id).await;
        let cover_file = fetch_file(&state, &cover_page.sha3_512_hashs.first().unwrap()).await;
        work_map.insert("id", work.id.to_string());
        work_map.insert("cover_url", cover_file.file_path.to_string());
        work_map.insert("name", work.book_title.to_string());
        works_map.push(work_map);
    }

    let mut context = Context::new();
    context.insert("works", &works_map);

    // let footer: String = format!("Powered by tuzuri version {VERSION}, {LICENSE_NOTICE}");
    context.insert("f", &footer());

    let rendered = tera.render("index.html", &context).unwrap();
    HttpResponse::Ok().body(rendered)
}

async fn images(
    tera: web::Data<Tera>,
    data: web::Data<AppState>,
    id: web::Path<i32>,
) -> impl Responder {
    let state: Pool<Postgres> = data.db.clone();
    let id: i32 = id.into_inner();
    let work_braid: BraidWork = fetch_work(&state, &id).await;

    let mut page_braids: Vec<BraidPage> = Vec::new();
    for page_braid_id in work_braid.braid_ids {
        page_braids.push(fetch_page_braids(&state, &page_braid_id).await);
    }

    let mut files: Vec<(i32, File)> = Vec::new();
    for page_braid in page_braids {
        files.push((
            page_braid.id,
            fetch_file(&state, &page_braid.sha3_512_hashs.first().unwrap()).await,
        ));
    }

    let mut pages = Vec::new();
    for (id, file) in files {
        let mut page = HashMap::new();
        page.insert("braidid", id.to_string());
        page.insert("hash", hex::encode(file.sha3_512));
        page.insert("url", file.file_path);
        page.insert("capacity", file.capacity_byte.to_string());
        page.insert("mime", file.mime_type + "/" + &file.mime_subtype);
        pages.push(page);
    }

    let mut context = Context::new();
    context.insert("name", &work_braid.book_title);
    context.insert("pages", &pages);
    context.insert("f", &footer());

    let rendered = tera.render("images.html", &context).unwrap();
    HttpResponse::Ok().body(rendered)
}

async fn quick_scan_api(data: web::Data<AppState>) -> impl Responder {
    let new_works: Vec<FileInfo> = quick_scan(&data.db).await;
    db::insert_files(&data.db, new_works).await;

    HttpResponse::Ok()
}

async fn full_scan_api(data: web::Data<AppState>) -> impl Responder {
    let new_files: Vec<FileInfo> = full_scan(&data.db).await;
    db::insert_files(&data.db, new_files).await;

    HttpResponse::Ok()
}

async fn fetch_page_braid_data(data: web::Data<AppState>, id: web::Path<i32>) -> impl Responder {
    let book_braid_id: i32 = id.into_inner();

    match sqlx::query_as::<_, BraidPage>(
        r#"
        SELECT * FROM braids WHERE id = $1;
    "#,
    )
    .bind(book_braid_id)
    .fetch_one(&data.db)
    .await
    {
        Ok(page_braid) => HttpResponse::Ok().json(page_braid),
        Err(err) => panic!("error fetch_page_braids {}", err),
    }
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await
        .expect("Error building a connection pool");

    let tera = Tera::new("templates/**/*").unwrap();

    db::insert_files(&pool, quick_scan(&pool).await).await;

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(tera.clone()))
            .app_data(web::Data::new(AppState { db: pool.clone() }))
            .route("/", web::get().to(index))
            .route("/{id}", web::get().to(images))
            .route("/api/v1/quick-scan", web::get().to(quick_scan_api))
            .route("/api/v1/full-scan", web::get().to(full_scan_api))
            .route(
                "/api/v1/page-data/{id}",
                web::get().to(fetch_page_braid_data),
            )
            .service(actix_files::Files::new("/works", "works").show_files_listing())
            .service(actix_files::Files::new("/js", "static/js").show_files_listing())
            .service(actix_files::Files::new("/css", "static/css").show_files_listing())
    })
    .bind(("0.0.0.0", 2261))?
    .run()
    .await
}
