use std::fs::File;
use std::fs::OpenOptions;
use std::io::BufReader;
use std::path::{Path, PathBuf};
use std::{fs, io};

use file_format::FileFormat;
use natord;
use serde::{Deserialize, Serialize};
use serde_json;
use sha3::{Digest, Sha3_512};
use sqlx::{self, Pool, Postgres};

use std::collections::HashMap;

use crate::db::*;

#[derive(Debug, Serialize, Deserialize)]
pub struct FileInfo {
    pub path: PathBuf,
    pub mime_type: String,
    pub mime_subtype: String,
    pub capacity_byte: u64,
    pub sha3_512: Vec<u8>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct BraidInfo {
    pub kind: String,
    pub braid_ids: Vec<i32>,
    pub author_ids: Vec<i32>,
    pub organization_ids: Vec<i32>,
    pub user_tag_ids: Vec<i32>,
    pub user_comment: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserTag {
    pub id: i32,
    pub tag_name: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Author {
    pub id: i32,
    pub author: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Organizations {
    pub id: i32,
    pub organization: String,
}

pub fn read_dir_entries<P: AsRef<Path>>(path: P) -> io::Result<Vec<PathBuf>> {
    let mut entries = fs::read_dir(path)?
        .map(|res| res.map(|e| e.path()))
        .collect::<Result<Vec<_>, io::Error>>()?;

    entries.sort_by(|a, b| natord::compare(&a.to_str().unwrap(), &b.to_str().unwrap()));

    Ok(entries)
}

pub fn hard_scan() -> Vec<FileInfo> {
    // all files hash scan
    println!("hard scanning...");

    let authors = scan_authors();
    let works = scan_works(&authors);
    let files = scan_files(&works);
    let files_info: Vec<FileInfo> = get_files_info(files);

    println!("scaned");

    return files_info;
}

pub async fn full_scan(state: &Pool<Postgres>) -> Vec<FileInfo> {
    // all files scan
    println!("full scanning...");

    let authors = scan_authors();
    let works = scan_works(&authors);
    let files = scan_files(&works);
    let new_files = find_new_files(state, &files).await;
    let files_info: Vec<FileInfo> = get_files_info(new_files);

    println!("scaned");

    return files_info;
}

pub async fn quick_scan(state: &Pool<Postgres>) -> Vec<FileInfo> {
    // works dirctory scan
    println!("quick scanning...");

    let authors = scan_authors();
    let works = scan_works(&authors);
    let new_works = find_new_works(state, &works).await;
    let new_files = scan_files(&new_works);
    let new_files_info: Vec<FileInfo> = get_files_info(new_files);
    println!("quick scaned");

    return new_files_info;
}

async fn find_new_works(state: &Pool<Postgres>, works: &Vec<PathBuf>) -> Vec<PathBuf> {
    let mut new_work: Vec<PathBuf> = Vec::new();
    for work in works.iter() {
        if is_work_exist(state, work).await {
            continue;
        } else {
            new_work.push(work.to_path_buf());
        }
    }
    println!("{:#?}", &new_work);
    return new_work;
}

async fn find_new_files(state: &Pool<Postgres>, files: &Vec<PathBuf>) -> Vec<PathBuf> {
    let mut new_files: Vec<PathBuf> = Vec::new();
    for file in files.iter() {
        if is_file_exist(state, file).await {
            continue;
        } else {
            new_files.push(file.to_path_buf());
        }
    }
    println!("{:#?}", &new_files);
    return new_files;
}

fn scan_authors() -> Vec<PathBuf> {
    println!("authors scanning...");

    let authors: Vec<PathBuf> =
        read_dir_entries("works").expect("Failed to read authors directory");
    println!("authors scaned");
    println!("{:?}", authors);

    return authors;
}

fn scan_works(authors: &Vec<PathBuf>) -> Vec<PathBuf> {
    println!("works scanning...");

    let mut works: Vec<PathBuf> = Vec::new();
    for author in authors.iter() {
        let this_author_works = read_dir_entries(&author).expect("Failed to read works directory");
        for work in this_author_works.iter() {
            works.push(work.to_path_buf());
        }
    }
    println!("works scaned");
    println!("{:?}", works);

    return works;
}

fn scan_files(works: &Vec<PathBuf>) -> Vec<PathBuf> {
    println!("files scanning...");

    let mut files: Vec<PathBuf> = Vec::new();
    for work in works.iter() {
        let this_work_files = read_dir_entries(&work).expect("Failed to read files");
        for file in this_work_files.iter() {
            files.push(file.to_path_buf());
        }
    }
    println!("files scand");
    return files;
}

fn get_files_info(files: Vec<PathBuf>) -> Vec<FileInfo> {
    let mut files_info: Vec<FileInfo> = Vec::new();
    for file in files.iter() {
        if is_ignore(&file) {
            println!("ignore: {:?}", &file);
            continue;
        } else {
            files_info.push(get_file_info(file));
        }
    }
    return files_info;
}

fn get_file_info(file_path: &PathBuf) -> FileInfo {
    let format = FileFormat::from_file(file_path).unwrap();
    let mime: Vec<&str> = format.media_type().split("/").collect();

    FileInfo {
        path: file_path.clone(),
        capacity_byte: fs::metadata(file_path).unwrap().len(),
        mime_type: mime[0].to_string(),
        mime_subtype: mime[1].to_string(),
        sha3_512: to_sha3_512(file_path),
    }
}

fn is_ignore(path: &PathBuf) -> bool {
    let file_name_first = path
        .file_name()
        .unwrap()
        .to_str()
        .unwrap()
        .chars()
        .next()
        .unwrap();

    if file_name_first == '.' {
        return true;
    } else if file_name_first == '_' {
        return true;
    } else if path.file_name().unwrap() == "_meta.json" {
        return true;
    } else {
        return false;
    }
}

pub fn to_sha3_512(file_path: &PathBuf) -> Vec<u8> {
    let mut file = File::open(file_path).unwrap();
    let mut hasher = Sha3_512::new();
    let _ = io::copy(&mut file, &mut hasher);
    hasher.finalize().to_vec()
}

pub fn read_meta(dir_path: &PathBuf) -> HashMap<String, String> {
    let metafile_path = dir_path.join("_meta.json");
    if metafile_path.is_file() {
        return read_meta_json(&metafile_path);
    } else {
        return read_meta_dirname(dir_path);
    }
}

pub fn read_meta_json(file_path: &PathBuf) -> HashMap<String, String> {
    let meta_file = OpenOptions::new()
        .read(true)
        .open(file_path.to_str().unwrap())
        .expect("error read _meta.json");

    let reader = BufReader::new(meta_file);

    let metadata: HashMap<String, String> = serde_json::from_reader(reader).unwrap();
    return metadata;
}

pub fn read_meta_dirname(dir_path: &PathBuf) -> HashMap<String, String> {
    let path_vec = dir_path.components().collect::<Vec<_>>();
    let mut meta_map = HashMap::new();
    let path_vec_last = path_vec.len() - 1;
    let path_vec_2nd_last = path_vec.len() - 2;
    let authors = path_vec[path_vec_2nd_last].as_os_str().to_str().unwrap();
    let title = path_vec[path_vec_last].as_os_str().to_str().unwrap();
    meta_map.insert("authors".to_string(), authors.to_string());
    meta_map.insert("organization".to_string(), authors.to_string());
    meta_map.insert("title".to_string(), title.to_string());

    return meta_map;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_dir_entries() {
        let path = Path::new("works_sample");
        let entries = read_dir_entries(path).expect("Failed to read directory");

        for entry in &entries {
            println!("{:?}", entry);
        }

        assert_eq!(entries.len(), 5);
    }

    #[test]
    fn test_full_scan() {
        let files = full_scan();
        println!("{:?}", files);
    }
}
