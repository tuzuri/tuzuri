use std::collections::HashMap;
use std::env;
use std::process;

pub fn footer() -> HashMap<String, String> {
    let mut footer = HashMap::new();

    footer.insert(
        String::from("wikipedia"),
        String::from("https://en.wikipedia.org/wiki/Free_software"),
    );
    footer.insert(
        String::from("license_name"),
        String::from("GNU Affero General Public License version 3"),
    );
    footer.insert(
        String::from("license_url"),
        String::from("https://www.gnu.org/licenses/agpl-3.0.html"),
    );

    let host_domain = match env::var("HOST_NAME") {
        Ok(host) => host,
        Err(err) => {
            println!("HOST_NAME {}", err);
            println!("please set environment val (docker-compose.yml, etc.)");
            process::exit(1);
        }
    };

    footer.insert(String::from("host_domain"), String::from(host_domain));
    footer.insert(
        String::from("repo_url"),
        String::from("https://codeberg.org/tuzuri/tuzuri"),
    );
    footer.insert(String::from("repo_name"), String::from("tuzuri/tuzuri"));

    let version = env!("CARGO_PKG_VERSION");
    footer.insert(
        String::from("version_url"),
        String::from("https://codeberg.org/tuzuri/tuzuri/releases/tag/v".to_owned() + version),
    );
    footer.insert(String::from("version"), String::from(""));

    if footer["version"] == "" {
        footer.insert(String::from("version"), String::from(version));
    }

    return footer;
}
