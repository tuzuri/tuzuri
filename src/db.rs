use crate::scanner::*;
use imageinfo::ImageInfo;
use serde::{Deserialize, Serialize};
use sqlx::FromRow;
use sqlx::{self, Pool, Postgres};
use std::path::PathBuf;

pub async fn insert_files(state: &Pool<Postgres>, files: Vec<FileInfo>) {
    println!("inserting...");

    for file in files {
        if file_exists(state, &file).await {
            println!("already exists: {:?}", &file.path);
            continue;
        } else {
            println!("insert: {:?}", &file.path);
        }

        insert_file(state, &file).await;

        if &file.mime_type == "image" {
            insert_images(state, &file).await;
        }

        let work_dir = &file.path.parent().unwrap().to_path_buf();
        let meta = read_meta(&work_dir);
        println!("{:#?}", &meta);

        // TODO: add error handling
        // TODO: add mulit authors and organizations
        let author_id = insert_author(&state, &meta["authors"]).await;
        let organization_id = insert_organization(&state, &meta["organization"]).await;

        let book_braid_id: i32 =
            insert_book_braid(&state, &meta["title"], &author_id, &organization_id).await;
        let page_braid_id: i32 = insert_page_braid(&state, &file.sha3_512).await;

        println!("book: {}, page: {}", &book_braid_id, &page_braid_id);
        update_book_braid(&state, &book_braid_id, &page_braid_id).await;

        update_book_author(&state, &page_braid_id, &author_id).await;
        update_book_organization(&state, &page_braid_id, &organization_id).await;
    }
    println!("inserted");
}

pub async fn insert_file(state: &Pool<Postgres>, file: &FileInfo) {
    match sqlx::query(
        r#"
        INSERT INTO files (sha3_512, file_path, mime_type, mime_subtype, capacity_byte)
        VALUES ($1, $2, $3, $4, $5) RETURNING sha3_512;
    "#,
    )
    .bind(&file.sha3_512)
    .bind(&file.path.to_string_lossy())
    .bind(&file.mime_type)
    .bind(&file.mime_subtype)
    .bind(file.capacity_byte as i64)
    .fetch_one(state)
    .await
    {
        Ok(_) => println!("ok insert_file"),
        Err(_) => println!("error insert_file"),
    };
}

async fn file_exists(state: &Pool<Postgres>, file: &FileInfo) -> bool {
    match sqlx::query(
        r#"
        SELECT sha3_512 FROM files WHERE sha3_512 = $1;
    "#,
    )
    .bind(&file.sha3_512)
    .fetch_one(state)
    .await
    {
        Ok(_) => true,
        Err(_) => false,
    }
}

pub async fn is_work_exist(state: &Pool<Postgres>, work_path: &PathBuf) -> bool {
    let work_path_search_wd = work_path.to_string_lossy() + "%";
    match sqlx::query(
        r#"
        SELECT file_path FROM files WHERE file_path LIKE $1;
    "#,
    )
    .bind(work_path_search_wd)
    .fetch_one(state)
    .await
    {
        Ok(_) => true,
        Err(_) => false,
    }
}

pub async fn is_file_exist(state: &Pool<Postgres>, file_path: &PathBuf) -> bool {
    let file_path_search_wd = file_path.to_string_lossy() + "%";
    match sqlx::query(
        r#"
        SELECT file_path FROM files WHERE file_path LIKE $1;
    "#,
    )
    .bind(file_path_search_wd)
    .fetch_one(state)
    .await
    {
        Ok(_) => true,
        Err(_) => false,
    }
}

fn image_dimensions(path: &PathBuf) -> (i64, i64) {
    let info = ImageInfo::from_file_path(&path.to_str().unwrap()).unwrap();
    return (info.size.width, info.size.height);
}

pub async fn insert_images(state: &Pool<Postgres>, file: &FileInfo) {
    let (width, height) = image_dimensions(&file.path);
    match sqlx::query(
        r#"
        INSERT INTO images (sha3_512, dimension_height, dimension_width)
        VALUES ($1, $2, $3) RETURNING sha3_512;
    "#,
    )
    .bind(&file.sha3_512)
    .bind(height as i32)
    .bind(width as i32)
    .fetch_one(state)
    .await
    {
        Ok(_) => println!("ok insert_images"),
        Err(_) => println!("error insert_images"),
    }
}

#[derive(Debug, Serialize, FromRow)]
pub struct Id {
    pub id: i32,
}

pub async fn insert_page_braid(state: &Pool<Postgres>, sha3_512: &Vec<u8>) -> i32 {
    let kind = "page";

    // insert braids kind
    let page_braid: i32 = match sqlx::query_as::<_, Id>(
        r#"
        INSERT INTO braids (kind) 
        VALUES ($1) RETURNING id;
    "#,
    )
    .bind(kind)
    .fetch_one(state)
    .await
    {
        Ok(row) => row.id,
        Err(err) => panic!("error insert_braid {}", err),
    };

    // update sha3_512_hashs
    match sqlx::query(
        r#"
        UPDATE braids SET sha3_512_hashs = array_append(sha3_512_hashs, $1) WHERE id = $2;
    "#,
    )
    .bind(&sha3_512)
    .bind(&page_braid)
    .execute(state)
    .await
    {
        Ok(_) => println!("ok update sha3_512_hashs"),
        Err(err) => panic!("error update sha3_512_hashs {}", err),
    };

    // update braid_ids
    match sqlx::query(
        r#"
        UPDATE braids SET braid_ids = array_append(braid_ids, $1) WHERE id = $2;
    "#,
    )
    .bind(&page_braid)
    .bind(&page_braid)
    .execute(state)
    .await
    {
        Ok(_) => println!("ok update braid_ids"),
        Err(err) => panic!("error update_braid {}]", err),
    };

    return page_braid;
}

pub async fn insert_book_braid(
    state: &Pool<Postgres>,
    title: &String,
    author_id: &i32,
    organization_id: &i32,
) -> i32 {
    let kind = "book";

    match sqlx::query_as::<_, Id>(
        r#"
        SELECT id FROM braids WHERE book_title = $1;
    "#,
    )
    .bind(title)
    .fetch_one(state)
    .await
    {
        Ok(row) => row.id,
        Err(_) => match sqlx::query_as::<_, Id>(
            r#"
            INSERT INTO braids (kind, book_title) 
            VALUES ($1, $2) RETURNING id;
    "#,
        )
        .bind(&kind)
        .bind(&title)
        .fetch_one(state)
        .await
        {
            Ok(row) => {
                update_book_author(&state, &row.id, &author_id).await;
                update_book_organization(&state, &row.id, &organization_id).await;
                row.id
            }
            Err(err) => panic!("error insert_book_braid {}", err),
        },
    }
}

pub async fn update_book_braid(state: &Pool<Postgres>, book_braid_id: &i32, page_braid_id: &i32) {
    match sqlx::query(
        r#"
        UPDATE braids SET braid_ids = array_append(braid_ids, $1) WHERE id = $2;
    "#,
    )
    .bind(page_braid_id)
    .bind(book_braid_id)
    .execute(state)
    .await
    {
        Ok(_) => println!("ok update update_book_braid"),
        Err(err) => panic!("error update_book_braid {}", err),
    };
}

pub async fn update_book_author(state: &Pool<Postgres>, braid_id: &i32, author_id: &i32) {
    match sqlx::query(
        r#"
        UPDATE braids SET author_ids = array_append(author_ids, $1) WHERE id = $2;
    "#,
    )
    .bind(&author_id)
    .bind(&braid_id)
    .execute(state)
    .await
    {
        Ok(_) => println!("ok update_book_author"),
        Err(err) => panic!("error update_book_author {}", err),
    };
}

pub async fn update_book_organization(
    state: &Pool<Postgres>,
    braid_id: &i32,
    organization_id: &i32,
) {
    match sqlx::query(
        r#"
        UPDATE braids SET organization_ids = array_append(organization_ids, $1) WHERE id = $2;
    "#,
    )
    .bind(&organization_id)
    .bind(&braid_id)
    .execute(state)
    .await
    {
        Ok(_) => println!("ok update_book_organization"),
        Err(err) => panic!("error update_book_organization {}", err),
    };
}

pub async fn insert_author(state: &Pool<Postgres>, author: &String) -> i32 {
    match sqlx::query_as::<_, Id>(
        r#"
        SELECT id FROM authors WHERE author_name = $1;
    "#,
    )
    .bind(author)
    .fetch_one(state)
    .await
    {
        Ok(row) => row.id,
        Err(_) => match sqlx::query_as::<_, Id>(
            r#"
        INSERT INTO authors (author_name) 
        VALUES ($1) RETURNING id;
    "#,
        )
        .bind(author)
        .fetch_one(state)
        .await
        {
            Ok(row) => row.id,
            Err(err) => panic!("error insert_author {}", err),
        },
    }
}

pub async fn insert_organization(state: &Pool<Postgres>, organization: &String) -> i32 {
    match sqlx::query_as::<_, Id>(
        r#"
        SELECT id FROM organizations WHERE organization_name = $1;
    "#,
    )
    .bind(organization)
    .fetch_one(state)
    .await
    {
        Ok(row) => row.id,
        Err(_) => match sqlx::query_as::<_, Id>(
            r#"
        INSERT INTO organizations (organization_name) 
        VALUES ($1) RETURNING id;
    "#,
        )
        .bind(organization)
        .fetch_one(state)
        .await
        {
            Ok(row) => row.id,
            Err(err) => panic!("error insert_organization {}", err),
        },
    }
}

// TODO: join BraidWork and BraidPage
#[derive(Debug, Serialize, Deserialize, FromRow)]
pub struct BraidWork {
    pub id: i32,
    pub kind: String,
    pub braid_ids: Vec<i32>,
    pub author_ids: Vec<i32>,
    pub organization_ids: Vec<i32>,
    pub user_tag_ids: Option<Vec<i32>>,
    pub book_title: String,
    pub user_comment: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, FromRow)]

pub struct BraidPage {
    pub id: i32,
    pub kind: String,
    pub sha3_512_hashs: Vec<Vec<u8>>,
    pub braid_ids: Vec<i32>,
    pub author_ids: Vec<i32>,
    pub organization_ids: Vec<i32>,
    pub user_tag_ids: Option<Vec<i32>>,
    pub book_title: Option<String>,
    pub user_comment: Option<String>,
}

pub async fn fetch_works(state: &Pool<Postgres>) -> Vec<BraidWork> {
    match sqlx::query_as::<_, BraidWork>(
        r#"
        SELECT * FROM braids WHERE kind = 'book';
    "#,
    )
    .fetch_all(state)
    .await
    {
        Ok(row) => row,
        Err(err) => panic!("error fetch_works {}", err),
    }
}

pub async fn fetch_work(state: &Pool<Postgres>, book_braid_id: &i32) -> BraidWork {
    match sqlx::query_as::<_, BraidWork>(
        r#"
        SELECT * FROM braids WHERE id = $1;
    "#,
    )
    .bind(book_braid_id)
    .fetch_one(state)
    .await
    {
        Ok(row) => row,
        Err(err) => panic!("error fetch_works {}", err),
    }
}

pub async fn fetch_page_braids(state: &Pool<Postgres>, book_braid_id: &i32) -> BraidPage {
    match sqlx::query_as::<_, BraidPage>(
        r#"
        SELECT * FROM braids WHERE id = $1;
    "#,
    )
    .bind(book_braid_id)
    .fetch_one(state)
    .await
    {
        Ok(row) => row,
        Err(err) => panic!("error fetch_page_braids {}", err),
    }
}

pub async fn fetch_page_braid(state: &Pool<Postgres>, page_braid_id: &i32) -> BraidPage {
    match sqlx::query_as::<_, BraidPage>(
        r#"
        SELECT * FROM braids WHERE id = $1;
    "#,
    )
    .bind(page_braid_id)
    .fetch_one(state)
    .await
    {
        Ok(row) => row,
        Err(err) => panic!("error fetch_page_braids {}", err),
    }
}

#[derive(Debug, Serialize, Deserialize, FromRow)]
pub struct File {
    pub sha3_512: Vec<u8>,
    pub file_path: String,
    pub mime_type: String,
    pub mime_subtype: String,
    pub capacity_byte: i32,
    // TODO: add DateTime in Serialize
    // pub added_at: Option<DateTime<Utc>>,
    pub source: Option<String>,
}

pub async fn fetch_file(state: &Pool<Postgres>, sha3_512: &Vec<u8>) -> File {
    match sqlx::query_as::<_, File>(
        r#"
        SELECT * FROM files WHERE sha3_512 = $1;
    "#,
    )
    .bind(sha3_512)
    .fetch_one(state)
    .await
    {
        Ok(row) => row,
        Err(err) => panic!("error fetch_file {}", err),
    }
}
