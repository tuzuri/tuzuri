# tuzuri
tuzuri is free software for creating, organizing and publishing works.

**Currently tuzuri is in the prototype stage and is not recommended for use other than for experimentation.**  
**Currently it is only possible to view works and their metadata.**  
**db will undergo breaking changes in the future.**

![screenshot_index](screenshot/tuzuri.png)

"[THE PARTY IS OVER, HUMAN!](https://www.flickr.com/photos/andymiccone/27445264122/)" by [Andy Miccone](https://www.flickr.com/photos/andymiccone/) is licensed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)

## Install
### docker compose
```
git clone https://codeberg.org/tuzuri/tuzuri.git
cd tuzuri
cp docker-compose_sample.yml docker-compose.yml
vi +/HOST_NAME docker-compose.yml # set your host name
mv works_sample works
docker compose up
open localhost:2261
```

## Usage
![screenshot_index](screenshot/tuzuri-index.png)

- Full Scan (All files scanning)
  - click two circular arrows icon
- Quick Scan (Work directories scanning)
  - click one circular arrow icon

![screenshot_page_view](screenshot/tuzuri-page-view.png)

- Go to next page
  - click yellow area
- Go to previous page
  - click blue area
- Go to nearby page
  - click green area (thumbnail)
- Go any page
  - click current page number (orange area), input any page number and press enter-key

## Shortcut
- <kbd>J</kbd>, <kbd>→</kbd>: next page
- <kbd>K</kbd>, <kbd>←</kbd>: prew page
- <kbd>B</kbd>: back top page
- <kbd>L</kbd>: love
- <kbd>N</kbd>: input page number
- <kbd>V</kbd>: view current page
- <kbd>T</kbd>: view thumbnails
- <kbd>I</kbd>: view image metadata
- <kbd>P</kbd>: view page metadata

## Directory Structure
- works/
  - author1/
  - author2/
    - work1/
    - work2/
      - page_1.png
      - page_2.png
      - page_3.png
      - _meta.json (optional)

## Used Technology

| name       | description          |
|------------|----------------------|
| actix-web  | web framework        |
| Tera       | html template engine |
| PostgreSQL | database             |
| sqlx       | sql tool kit         |
| webpack    | ts module bundler    |

## Support Format
- browser support image format
  - apng, avif, avif-sequence, bmp, gif, jpeg, png, svg, webp, webp (animated)

## Icons License
- "[Evericons version 1.1](http://evericons.com)" by [Aleksey Popov](http://alekseypopov.com) is licensed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)

## License
tuzuri  
Copyright (C) 2023-2024 Coro365 <coro.365@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.