FROM rust:1.70.0

WORKDIR /usr/src/tuzuri
COPY . .

RUN cargo install --path .

EXPOSE 2261
CMD ["tuzuri"]
